package com.training.spring;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.training.spring.model.Customer;
import com.training.spring.model.Dish;
import com.training.spring.service.CustomerService;
import com.training.spring.service.DishService;


@Controller
public class CustomerController {
	
	//private PersonService personService;
	private CustomerService customerService;
	@Autowired
	private DishService dishService;
	
	@Autowired(required=true)
	@Qualifier(value="customerService")
	public void setCustomerService(CustomerService ps){
		this.customerService = ps;
	}
	
	@RequestMapping(value = "table")
	public String listPersons2(Model model) {
		//model.addAttribute("person", new Person());
		model.addAttribute("customer",new Customer());
		//model.addAttribute("listPersons", this.personService.listPersons());
		return "table";
	}
	
	@RequestMapping(value = "/se")
	public String listPersons24(Model model) {
		//model.addAttribute("person", new Person());
		//model.addAttribute("customer",new Customer());
		//model.addAttribute("listPersons", this.personService.listPersons());
		return "final";
	}
	
	@RequestMapping(value = "/")
	public String listPersons(Model model) {
		//model.addAttribute("person", new Person());
		model.addAttribute("customer",new Customer());
		//model.addAttribute("listPersons", this.personService.listPersons());
		return "index";
	}
	
//	//For add and update person both
//	@RequestMapping(value= "/person/add", method = RequestMethod.POST)
//	public String addPerson(@ModelAttribute("person") Person p){
//		System.out.println("#####product :"+p);
//			this.personService.addPerson(p);
//		return "redirect:/persons";

//	}

	//For add and update person both
	
	
	
	@RequestMapping(value= "/customer/add", method = RequestMethod.POST)
	public String addCustomer(@ModelAttribute("customer") Customer p) {
		//model.addAttribute("person", new Person());
		//model.addAttribute("person", new Person());
		//Session session=new AnnotationConfiguration().configure().buildSessionFactory().openSession();
		//session.add
		System.out.println(p);
		
		this.customerService.addCustomer(p);
		return "redirect:/menu";
	}
	@RequestMapping(value= "/customer/verify", method = RequestMethod.POST)
	public String verifyCustomer(@ModelAttribute("customer") Customer p)
	{
		
		ModelAndView view = new ModelAndView();


		String ph = p.getCustPhone();
		if(this.customerService.isCustomer(ph))
		
			{
			return "redirect:/menu";
			}else
			{//view.setViewName("Customer")
				return "Customer";
			}
			}
		


		@RequestMapping(value= "/menu", method = RequestMethod.GET)
		public ModelAndView menu(Model model)
		{
			
			ModelAndView view = new ModelAndView();
			model.addAttribute("menu",dishService.listAllItems());

			//String ph = p.getCustPhone();
			//if(this.customerService.isCustomer(ph))
			
				//{
				//return "redirect:/menu";
//				}else
//				{//view.setViewName("Customer");}
//			return "Customer";
			view.setViewName("Menu");
			return view;

		}

	
//	
//	@RequestMapping("/remove/{id}")
//    public String removePerson(@PathVariable("id") int id){
//		
//        this.personService.removePerson(id);
//        return "redirect:/persons";
//    }
// 
//    @RequestMapping("/edit/{id}")
//    public String editPerson(@PathVariable("id") int id, Model model){
//        model.addAttribute("person", this.personService.getPersonById(id));
//        model.addAttribute("listPersons", this.personService.listPersons());
//        return "person";
//    }
//		@RequestMapping("/remove/{id}")
//	    public String removePerson(@PathVariable("id") int id){
//			
//	        this.personService.removePerson(id);
//	        return "redirect:/persons";
//	    }
	// 
	    @RequestMapping("/order")
	    public void editPerson( Model model){
	        System.out.println(model);
	    }
}
