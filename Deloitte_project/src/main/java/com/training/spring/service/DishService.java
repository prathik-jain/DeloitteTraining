package com.training.spring.service;

import java.util.List;

import com.training.spring.model.Customer;
import com.training.spring.model.Dish;

public interface DishService {
	
	public List<Dish> listAllItems();
	public List<Dish> listVegItems();
	public List<Dish> listNonItems();

}
