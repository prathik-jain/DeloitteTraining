package com.training.spring.service;

import com.training.spring.model.Customer;

import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.CustomerDAO;

public class CustomerServiceImpl implements CustomerService {

	private CustomerDAO CustomerDAO;
	
	 @Transactional
	@Override
	public Boolean isCustomer(String phone) {
		
		return CustomerDAO.isCustomer(phone);
	}

	public CustomerDAO getCustomerDAO() {
		return CustomerDAO;
	}

	public void setCustomerDAO(CustomerDAO customerDAO) {
		CustomerDAO = customerDAO;
	}
	 @Transactional
	@Override
	public int addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return CustomerDAO.addCustomer(customer);
	}
	 @Transactional
	@Override
	public Customer getCustomerByNumber(String phone) {
		// TODO Auto-generated method stub
		return CustomerDAO.getCustomerByNumber(phone);
	}

	@Override
	public int getMaxCustID() {
		// TODO Auto-generated method stub
		return CustomerDAO.getMaxCustID();
	}

}
