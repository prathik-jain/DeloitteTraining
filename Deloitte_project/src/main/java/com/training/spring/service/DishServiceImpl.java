package com.training.spring.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.DishDAO;
import com.training.spring.model.Customer;
import com.training.spring.model.Dish;

public class DishServiceImpl implements DishService {

	private DishDAO dishDAO;
	public DishDAO getDishDAO() {
		return dishDAO;
	}
	public void setDishDAO(DishDAO dishDAO) {
		this.dishDAO = dishDAO;
	}
	@Transactional
	@Override
	public List<Dish> listAllItems() {
		// TODO Auto-generated method stub
		return dishDAO.listAllItems();
	}
	@Transactional
	@Override
	public List<Dish> listVegItems() {
		// TODO Auto-generated method stub
		return dishDAO.listVegItems();
	}
	@Transactional
	@Override
	public List<Dish> listNonItems() {
		// TODO Auto-generated method stub
		return dishDAO.listNonItems();
	}
	


}
