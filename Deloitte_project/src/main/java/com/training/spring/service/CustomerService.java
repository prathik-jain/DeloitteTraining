package com.training.spring.service;

import com.training.spring.model.Customer;

public interface CustomerService {

	public Boolean isCustomer(String phone);
	public int addCustomer(Customer customer);
	public Customer getCustomerByNumber(String phone);
	public int getMaxCustID();
	
}
