package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Dish")
public class Dish {

	@Id
	private int itemId;
	@Column(length=2)
	private int catId;
	@Column(length=2)
	private int typeId;
	private int price;
	private String description;
	private int imageId;
	private int spicy;
	private String name;
	
	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Dish() {
		super();
	}

	public Dish(int itemId, int catId, int typeId, int price, String description, int imageId, int spicy) {
		super();
		this.itemId = itemId;
		this.catId = catId;
		this.typeId = typeId;
		this.price = price;
		this.description = description;
		this.imageId = imageId;
		this.spicy = spicy;
	}
	
	
	public int getItemId() {
		return itemId;
	}
	
	public int getCatId() {
		return catId;
	}
	
	public int getTypeId() {
		return typeId;
	}
	
	@Column
	public int getPrice() {
		return price;
	}
	
	@Column
	public String getDescription() {
		return description;
	}
	
	
	public int getImageId() {
		return imageId;
	}
	
	@Column
	public int getSpicy() {
		return spicy;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public void setSpicy(int spicy) {
		this.spicy = spicy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + catId;
		result = prime * result + imageId;
		result = prime * result + itemId;
		result = prime * result + price;
		result = prime * result + spicy;
		result = prime * result + typeId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dish other = (Dish) obj;
		if (catId != other.catId)
			return false;
		if (description != other.description)
			return false;
		if (imageId != other.imageId)
			return false;
		if (itemId != other.itemId)
			return false;
		if (price != other.price)
			return false;
		if (spicy != other.spicy)
			return false;
		if (typeId != other.typeId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Dish [itemId=" + itemId + ", catId=" + catId + ", typeId=" + typeId + ", price=" + price
				+ ", description=" + description + ", imageId=" + imageId + ", spicy=" + spicy + ", name=" + name + "]";
	}

	
	
	
}
