package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Status")
public class Status {
	
	@Id
	private int statusId;
	@Column
	private int statusValue;
	
	public Status() {
		super();
	}

	public Status(int statusId, int statusValue) {
		super();
		this.statusId = statusId;
		this.statusValue = statusValue;
	}
	
	@Id
	public int getStatusId() {
		return statusId;
	}
	
	@Column(length=20)
	public int getStatusValue() {
	return statusValue;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public void setStatusValue(int statusValue) {
		this.statusValue = statusValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + statusId;
		result = prime * result + statusValue;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Status other = (Status) obj;
		if (statusId != other.statusId)
			return false;
		if (statusValue != other.statusValue)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Status [statusId=" + statusId + ", statusValue=" + statusValue + "]";
	}
	
	
}
