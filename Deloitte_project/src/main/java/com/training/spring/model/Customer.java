package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Customer")
public class Customer {
	
	
	@Override
	public String toString() {
		return "Customer [ custPhone=" + custPhone + ", custName=" + custName + ", custEmail="
				+ custEmail + "]";
	}
	private String custPhone;
	private String custName;
	private String custEmail;

	public Customer() {
		super();
	}
	
	
	public Customer(int cutId ,String custPhone , String custName ,String email) {
		super();
		this.custEmail=email;
	//	this.custId=cutId;
		this.custName=custName;
		this.custPhone=custPhone;
	}
	
	
	   // @GeneratedValue
	
	@Id
	@Column(length=20)
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	
	@Column(length=20)
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
	@Column(length=20)
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}


}
