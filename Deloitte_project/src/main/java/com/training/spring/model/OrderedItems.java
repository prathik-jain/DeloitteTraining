package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OrderedItems")
public class OrderedItems {

	@Id
	private int orderId;
	@Column(length=20)
	private int itemName;
	@Column(length=20)
	private int statusId;
	
	public OrderedItems() {
		super();
	}

	public OrderedItems(int orderId, int itemName, int statusId) {
		super();
		this.orderId = orderId;
		this.itemName = itemName;
		this.statusId = statusId;
	}

	@Id
	public int getOrderId() {
		return orderId;
	}
	
	@Column(length=20)
	public int getItemName() {
		return itemName;
	}
	
	@Column(length=20)
	public int getStatusId() {
		return statusId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public void setItemName(int itemName) {
		this.itemName = itemName;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + itemName;
		result = prime * result + orderId;
		result = prime * result + statusId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderedItems other = (OrderedItems) obj;
		if (itemName != other.itemName)
			return false;
		if (orderId != other.orderId)
			return false;
		if (statusId != other.statusId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderedItems [orderId=" + orderId + ", itemName=" + itemName + ", statusId=" + statusId + "]";
	}
	
		
}
