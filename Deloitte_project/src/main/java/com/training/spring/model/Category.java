package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Category")
public class Category {
	
	@Id
	private int catId;
	@Column
	private int catName;
	
	public Category() {
		super();
	}

	public Category(int catId, int catName) {
		super();
		this.catId = catId;
		this.catName = catName;
	}
	
	@Id
	public int getCatId() {
		return catId;
	}
	
	@Column(length=20)
	public int getCatName() {
		return catName;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public void setCatName(int catName) {
		this.catName = catName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + catId;
		result = prime * result + catName;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (catId != other.catId)
			return false;
		if (catName != other.catName)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [catId=" + catId + ", catName=" + catName + "]";
	}
	
	
	
}
