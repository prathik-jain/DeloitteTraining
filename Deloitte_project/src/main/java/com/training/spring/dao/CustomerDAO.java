package com.training.spring.dao;

import com.training.spring.model.Customer;

public interface CustomerDAO {
	
	public Boolean isCustomer(String phone);
	public int addCustomer(Customer customer);
	public Customer getCustomerByNumber(String phone);
	public int getMaxCustID();

}
