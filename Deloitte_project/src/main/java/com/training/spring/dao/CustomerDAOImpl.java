package com.training.spring.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.training.spring.model.Customer;


@Repository
public class CustomerDAOImpl implements CustomerDAO {

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public Boolean isCustomer(String phone) {
		
		Session session = this.sessionFactory.getCurrentSession();		
		Customer p = (Customer) session.get(Customer.class, new String(phone));
		//logger.info("Person loaded successfully, Person details="+p);
		if (p == null)
		return false;
		else
		return 	true;
	}

	@Override
	public int addCustomer(Customer customer) {
	Session session = this.sessionFactory.getCurrentSession();
	session.save(customer);
		//logger.info("Person saved successfully, Person Details="+p)
		return 0;
	}

	@Override
	public Customer getCustomerByNumber(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxCustID() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Query q = session.createQuery("MAX(custId) from Customer");	
		return q.getMaxResults();
		
	}

}
