package com.training.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.training.spring.model.Dish;


public class DishDAOImpl implements DishDAO {

	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Dish> listAllItems() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Dish> dishList = session.createQuery("from Dish").list();
		System.out.println(dishList);
		return dishList;
		
		
		}
	

	@Override
	public List<Dish> listVegItems() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Dish> dishList = session.createQuery("from Dish where typeId='1'").list();
		
		return dishList;
	}

	@Override
	public List<Dish> listNonItems() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Dish> dishList = session.createQuery("from Dish where typeId='2'").list();
		return dishList;
		}

}
