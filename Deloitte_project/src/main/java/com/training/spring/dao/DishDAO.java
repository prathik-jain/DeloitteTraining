package com.training.spring.dao;

import java.util.List;

import com.training.spring.model.Dish;

public interface DishDAO {
	
	public List<Dish> listAllItems();
	public List<Dish> listVegItems();
	public List<Dish> listNonItems();

}
