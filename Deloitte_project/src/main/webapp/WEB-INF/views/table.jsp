<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="resources/assets/css/material-kit.css" rel="stylesheet" />
<title>Verify</title>
<!--<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>			  -->
</head>
<body>
<div class="container">

	    <div class="section section-signup page-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 ml-auto mr-auto">
            <div class="card card-login">
              <div class="card-header card-header-warning text-center">
                  <h4 class="card-title">Let's get started.</h4>

                </div>

                <div class="card-body">
				  
				  <br><br>
				
				  <center><p class="text-danger">We need your phone number to check if you have previously visited our restaurant. If you haven't, this will be your login hereafter.</p>	</center>	
                  <div class="input-group">			  
  
                      <c:url var="addAction" value="/customer/verify" ></c:url>
                      
                          <form:form  class="form" action="${addAction}" commandName="customer">
        

                <div class="card-body">
				<br>
				
                  <div class="input-group">

                     <center> <span class="input-group-text has-success">
                        <i class="material-icons">phone_android</i>
                      </span>  </center> <br>

 
                    <form:input path="custPhone"  class="form-control"  pattern="[6-9]{1}[0-9]{9}" placeholder="Phone number"/>

                  </div>
				

                 

				  <br>
				  <input type="submit" class="btn btn-warning btn-wd btn-lg" value="Show me the menu!"/>
                </div>
                <div class="footer text-center">
                 <br/>
				  <a href="/Deloitte_project" class="btn btn-default btn-sm">Back</a>
                </div>
              </form:form>	
                  </div>
				
				  <br><br>
                </div>

            </div>
          </div>
        </div>
      </div>
    </div>	
	
	</div>
</body>
</html>