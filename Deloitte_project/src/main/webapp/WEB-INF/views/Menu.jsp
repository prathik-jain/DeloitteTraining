<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="./assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Menu
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="resources/material-kit.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./assets/demo/demo.css" rel="stylesheet" />

    <script type="text/javascript">
   
        
    </script>
    
    
    
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
    <div class="container">

        <div id="nav-tabs">
            <center><h3 class="title">Menu</h3></center>
            <div class="row">
                <div class="col-md-12">

                    <!-- Tabs with icons on Card -->
                    <div class="card card-nav-tabs">
                        <div class="card-header card-header-rose">
                            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">
                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#starters" data-toggle="tab">
                                                <i class="material-icons">fastfood</i> Starters
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#maincourse" data-toggle="tab">
                                                <i class="material-icons">restaurant</i> Main Course
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#desserts" data-toggle="tab">
                                                <i class="material-icons">cake</i> Desserts
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#drinks" data-toggle="tab">
                                                <i class="material-icons">local_bar</i> Drinks And Beverages
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card-body ">
                            <div class="tab-content text-center">
                                <div class="tab-pane active" id="starters">
                                
  <div class="container">
   <c:forEach items="${menu}" var="item">
   <c:if test="${item.catId == 1 }">
                                        <div class="row">
                                            <div class="card">
                                                <div class="card-body">
                                               
                                                    <div class="row">
                                                        <div class="col-sm-4 col-md-4 col-lg-3">
                                                            <div style="text-align: center;">
                                                                <img src="${item.name}.jpg" style="max-width: 100%; height: auto;"
                                                                    alt="Your Profile Photo" />
                                                            </div>

                                                         
                                                        </div>
                                                        <div class="col-sm-8 col-md-8 col-lg-7">
                                                            <h4 class="card-title">${item.name}</h4>
                                                            <p class="card-text">${item.description}.</p>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-2 align-middle">
                                                            <h3 class="card-title">&#8377;&nbsp;${item.price}</h3>
                                                            <div class="">
																<br>
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="decreaseQty(${item.itemId})">
                                                                    <i class="material-icons">remove</i>
                                                                </button>
                                                                &nbsp;
                                                                <span id="${item.itemId}">
                                                                    0
                                                                </span>
                                                                &nbsp;
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="increaseQty(${item.itemId})">
                                                                    <i class="material-icons">add</i>
                                                                </button>

                        
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                     </div>
                                                     </div>
                                                     </div>
                                                      </c:if>
                                                      </c:forEach>

                                                </div>
                                        	

                                </div>
                                <div class="tab-pane" id="maincourse">

                                  <div class="container">
   <c:forEach items="${menu}" var="item">
   <c:if test="${item.catId == 2 }">
                                        <div class="row">
                                            <div class="card">
                                                <div class="card-body">
                                               
                                                    <div class="row">
                                                        <div class="col-sm-4 col-md-4 col-lg-3">
                                                            <div style="text-align: center;">
                                                                <img src="cr.jpg" style="max-width: 100%; height: auto;"
                                                                    alt="Your Profile Photo" />
                                                            </div>

                                                            <!--	<img class="card-img-top" src="cr.jpg" alt="Card image" height="150" width="200">	-->
                                                        </div>
                                                        <div class="col-sm-8 col-md-8 col-lg-7">
                                                            <h4 class="card-title">${item.name}</h4>
                                                            <p class="card-text">${item.description}.</p>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-2 align-middle">
                                                            <h3 class="card-title">&#8377;&nbsp;${item.price}</h3>
                                                            <div class="">
																<br>
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="decreaseQty(${item.itemId})">
                                                                    <i class="material-icons">remove</i>
                                                                </button>
                                                                &nbsp;
                                                                <span id="${item.itemId}">
                                                                    0
                                                                </span>
                                                                &nbsp;
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="increaseQty(${item.itemId})">
                                                                    <i class="material-icons">add</i>
                                                                </button>

                        
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                     </div>
                                                     </div>
                                                     </div>
                                                      </c:if></c:forEach>

                                                </div>							

                                </div>
								
								
                                <div class="tab-pane" id="desserts">

                                   <div class="container">
   <c:forEach items="${menu}" var="item">
   <c:if test="${item.catId == 3 }">
                                        <div class="row">
                                            <div class="card">
                                                <div class="card-body">
                                               
                                                    <div class="row">
                                                        <div class="col-sm-4 col-md-4 col-lg-3">
                                                            <div style="text-align: center;">
                                                                <img src="cr.jpg" style="max-width: 100%; height: auto;"
                                                                    alt="Your Profile Photo" />
                                                            </div>

                                                            <!--	<img class="card-img-top" src="cr.jpg" alt="Card image" height="150" width="200">	-->
                                                        </div>
                                                        <div class="col-sm-8 col-md-8 col-lg-7">
                                                            <h4 class="card-title">${item.name}</h4>
                                                            <p class="card-text">${item.description}.</p>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-2 align-middle">
                                                            <h3 class="card-title">&#8377;&nbsp;${item.price}</h3>
                                                            <div class="">
																<br>
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="decreaseQty(${item.itemId})">
                                                                    <i class="material-icons">remove</i>
                                                                </button>
                                                                &nbsp;
                                                                <span id="${item.itemId}">
                                                                    0
                                                                </span>
                                                                &nbsp;
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="increaseQty(${item.itemId})">
                                                                    <i class="material-icons">add</i>
                                                                </button>

                        
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                     </div>
                                                     </div>
                                                     </div>
                                                      </c:if></c:forEach>

                                                </div>
                                </div>
                                <div class="tab-pane" id="drinks">
                                     <div class="container">
   <c:forEach items="${menu}" var="item">
   <c:if test="${item.catId == 4 }">
                                        <div class="row">
                                            <div class="card">
                                                <div class="card-body">
                                               
                                                    <div class="row">
                                                        <div class="col-sm-4 col-md-4 col-lg-3">
                                                            <div style="text-align: center;">
                                                                <img src="cr.jpg" style="max-width: 100%; height: auto;"
                                                                    alt="Your Profile Photo" />
                                                            </div>

                                                            <!--	<img class="card-img-top" src="cr.jpg" alt="Card image" height="150" width="200">	-->
                                                        </div>
                                                        <div class="col-sm-8 col-md-8 col-lg-7">
                                                            <h4 class="card-title">${item.name}</h4>
                                                            <p class="card-text">${item.description}.</p>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-2 align-middle">
                                                            <h3 class="card-title">&#8377;&nbsp;${item.price}</h3>
                                                            <div class="">
																<br>
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="${item.itemId}">
                                                                    <i class="material-icons">remove</i>
                                                                </button>
                                                                &nbsp;
                                                                <span id="${item.itemId}">
                                                                    0
                                                                </span>
                                                                &nbsp;
                                                                <button class="btn btn-rose btn-fab btn-fab-mini btn-round" onclick="${item.itemId}">
                                                                    <i class="material-icons">add</i>
                                                                </button>

                        
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                     </div>
                                                     </div>
                                                     </div>
                                                      </c:if></c:forEach>

                                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Tabs with icons on Card -->
						<center><a href="se" ><button class="btn btn-rose" onclick="">Order</button></a></center>
                </div>
            </div>
        </div>



        <!--   Core JS Files   -->
        <script src="resources/js/core/jquery.min.js" type="text/javascript"></script>
        <script src="resources/js/core/popper.min.js" type="text/javascript"></script>
        <script src="resources/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="resources/js/plugins/moment.min.js"></script>
        <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
        <script src="assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
        <!--	Plugin for Sharrre btn -->
        <script src="assets/js/plugins/jquery.sharrre.js" type="text/javascript"></script>
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="assets/js/material-kit.js?v=2.0.4" type="text/javascript"></script>
        <script>
        
        var db = openDatabase('mydb', '1.0', 'Test DB', 2 * 1024 * 1024);
        db.transaction(function (tx) {
            tx.executeSql('DROP TABLE ORDERS');
        });




    function addItem(id, qty) {
            db.transaction( function (tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS ORDERS (ID UNIQUE, QTY)');
                tx.executeSql(`INSERT OR REPLACE INTO ORDERS VALUES(${id},${qty})`);
            })
        }
        function increaseQty(id) {
            qty = document.getElementById(id);
            qty.innerText = parseInt(qty.innerText) == 10 ? '10' : parseInt(qty.innerText) + 1;
            if (parseInt(qty.innerText) > 0) {
                addItem(id, parseInt(qty.innerText));
            }

        }

        function decreaseQty(id) {
            qty = document.getElementById(id);
            qty.innerText = parseInt(qty.innerText) == 0 ? '0' : parseInt(qty.innerText) - 1;
            if (parseInt(qty.innerText) > 0) {
                addItem(id, parseInt(qty.innerText));
            }
        }

            function order() {
				
                db.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM ORDERS', [], function (tx, results) {
                    	tx.executeSql(`DROP TABLE ORDERS`);
                        let x = results.rows;
                        console.log(x);

                        const http = new XMLHttpRequest();
                        const url = './order';
                        http.open("GET", url);
                        http.send({ "body": x });

                    }, null)
                })
            }      
            
            $(document).ready(function () {
                //init DateTimePickers
                materialKit.initFormExtendedDatetimepickers();

                // Sliders Init
                materialKit.initSliders();
            });


            function scrollToDownload() {
                if ($('.section-download').length != 0) {
                    $("html, body").animate({
                        scrollTop: $('.section-download').offset().top
                    }, 1000);
                }
            }

        </script>


</body>
</html>