<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="resources/material-kit.css" rel="stylesheet" />


<title>Home</title>
</head>
<body>
<div class="container">

		    <div class="section section-signup page-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 ml-auto mr-auto">
            <div class="card card-login">
              <form class="form" method="" action="">
                <div class="card-header card-header-warning text-center">
                  <h2 class="card-title">Welcome!</h2>

                </div>

                <div class="card-body">
				  
				  <br><br>
                <div class="footer text-center">
					<a href="table" class="btn btn-warning btn-lg">
			
						<i class="material-icons">fastfood</i>&nbsp;&nbsp; Customer
				
					</a>
                </div>	
				<br>
                <div class="footer text-center">
					<a href="averify.html" class="btn btn-warning btn-lg">
						<i class="material-icons">account_box</i>&nbsp;&nbsp; Admin
					</a>
                </div>				
				<br>
                <div class="footer text-center">
					<a href="kverify.html" class="btn btn-warning btn-lg">
						<i class="material-icons">kitchen</i>&nbsp;&nbsp; Chef
					</a>
                </div>	
				<br>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>	
	
		
	</div>
	
	</div>
</body>
</html>