package exe;

public class Student {

	private English English;
	private Maths Maths;
	private Hindi Hindi;
	public English getEnglish() {
		return English;
	}
	public void setEnglish(English english) {
		English = english;
	}
	public Maths getMaths() {
		return Maths;
	}
	public void setMaths(Maths maths) {
		Maths = maths;
	}
	public Hindi getHindi() {
		return Hindi;
	}
	public void setHindi(Hindi hindi) {
		Hindi = hindi;
	}
	@Override
	public String toString() {
		return "Student English = " + English + ", Maths = " + Maths + ", Hindi = " + Hindi;
	}
	public Student(exe.English english, exe.Maths maths, exe.Hindi hindi) {
		super();
		English = english;
		Maths = maths;
		Hindi = hindi;
	}
	
	
}
