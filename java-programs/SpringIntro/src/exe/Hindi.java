package exe;

public class Hindi {
	private int Hmarks;
	private String Hgrade;
	public int getHmarks() {
		return Hmarks;
	}
	public void setHmarks(int hmarks) {
		Hmarks = hmarks;
	}
	public String getHgrade() {
		return Hgrade;
	}
	public void setHgrade(String hgrade) {
		Hgrade = hgrade;
	}
	@Override
	public String toString() {
		return "Hindi marks = " + Hmarks + ", grade = "  + Hgrade;
	}
	public Hindi(int hmarks, String hgrade) {
		super();
		Hmarks = hmarks;
		Hgrade = hgrade;
	}
	
	
	
}
