package exe;

import org.springframework.beans.factory.BeanFactory;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


import exe.Student;




public class Client {

	public static void main(String[] args) {

//		Customer customer = new Customer();
//		customer.setCustomerName("Rahul");
//		System.out.println(customer.getCustomerName());
		Student student;
		
		Resource resource = new ClassPathResource("beans.xml");
		BeanFactory factory = new XmlBeanFactory(resource);

		student = (Student) factory.getBean("student");
		System.out.println(student);


		
		
	}

}
