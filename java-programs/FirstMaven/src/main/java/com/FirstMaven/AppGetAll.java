package com.FirstMaven;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;


/**
 * Hello world!
 *
 */
public class AppGetAll 
{
    @SuppressWarnings("deprecation")
	public static void main( String[] args ) 
    {

    	Configuration config = new Configuration().configure();
    	SessionFactory factory = config.buildSessionFactory();
    	Session session = factory.openSession();
    	
    	Query q = session.createQuery("from Customer");
    	List<Customer> customers = q.list();
    	
    	Iterator<Customer> it = customers.iterator();
    	while(it.hasNext()) {
    		System.out.println(it.next());
    		
    		
    	}
    	
    	factory.close();
    	session.close();
    }
}
