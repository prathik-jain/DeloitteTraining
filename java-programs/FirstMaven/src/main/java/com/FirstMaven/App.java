package com.FirstMaven;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    @SuppressWarnings("deprecation")
	public static void main( String[] args ) 
    {
    	
//		Class.forName("oracle.jdbc.driver.OracleDriver");
//		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "scott", "tiger");
//    	Customer customer = new Customer(123, "prat", "BOM", 2500);
    	Customer customer = new Customer();
    
    	Configuration config = new Configuration().configure();
    	SessionFactory factory = config.buildSessionFactory();
    	
    	Session session = factory.openSession();
    	
    	Transaction transaction = session.beginTransaction();
    	
//    	session.save(customer);
    	customer = (Customer) session.get(Customer.class, 122);
    	
    	customer.setCustomerName("Macha");
    	customer.setCustomerAddress("NYC");
    	customer.setBillAmount(300);
    	
    	System.out.println(customer);
    	
    	session.update(new Customer(124,"DON","HYD",500000));
    	transaction.commit();
    	
    	System.out.println("Data updated");
    	factory.close();
    	session.close();
    }
}
