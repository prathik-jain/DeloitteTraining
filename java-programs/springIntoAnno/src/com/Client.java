package com;


import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import config.AppConfig;


public class Client {

	public static void main(String[] args) {
		
		Customer customer, customer1;
		BankAccount bankAccount;
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		
		customer = context.getBean(Customer.class);
		customer1 = context.getBean(Customer.class);
		
		customer.setCustomerName("Paresh");
		customer.setCustomerId(1);
		customer.setCustomerAddress("Dubai");
		customer.setBillAmount(500);
		bankAccount = context.getBean(BankAccount.class);
		bankAccount.setAccountNumber("12233");
		bankAccount.setBalance(500000);
//		
//		customer.setBankAccount(bankAccount);
		
		
		customer1.setCustomerId(2);
		customer1.setCustomerName("KP");
		customer1.setCustomerAddress("Mumbai");
		customer1.setBillAmount(500);
		customer1.setBankAccount(bankAccount);
		System.out.println(customer);
	}

}
