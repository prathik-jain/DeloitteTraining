package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.BankAccount;
import com.Customer;

@Configuration
public class AppConfig {


	@Bean
	@Scope("prototype")
	public Customer getCustomerObject() {
		return new Customer();
	}
	
	@Bean
	public BankAccount getBankAccountObject() {
		return new BankAccount();
	}
}
