package com.javatraining.customer.client;

import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {
		Customer customer = new Customer(999, "Sharukh", "Mumbai", 99000);
		
		CustomerDAO customerDAO = new CustomerDAOImpl();
		
//		Testing insert
		int result = customerDAO.insertCustomer(customer);
		System.out.println(result + " rows affected");
		
//		Testing update
		int updateResult = customerDAO.updateCustomer(999, "Bombay", 9000);
		System.out.println(updateResult + " rows affected");
		
//		Testing delete
		int deleteResult = customerDAO.deleteCustomer(999);
		System.out.println(deleteResult + " rows affected");
		
//		Testing findById
		Customer fCustomer = customerDAO.findByCustomerId(500);
		System.out.println(fCustomer);
		
//		Testing isCustomerExists
		boolean customerExists = customerDAO.isCustomerExists(550);
		System.out.println(customerExists ? "Exists" : "Does not exists");
		
//		Testing ListAllCustomers
		List<Customer> allCustomers = customerDAO.listAllCustomers();
		System.out.println(allCustomers);
	}

}
