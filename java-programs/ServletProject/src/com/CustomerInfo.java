package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class CustomerInfo
 */
public class CustomerInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int customerId = Integer.parseInt(req.getParameter("customerId"));
		String customerName = req.getParameter("customerName");
		String customerAddress =  req.getParameter("customerAddress");
		int billAmount = Integer.parseInt(req.getParameter("billAmount"));
		
		CustomerDAO customerDAO = new CustomerDAOImpl();
		Customer customer = new Customer(customerId, customerName, customerAddress, billAmount);
		int result = customerDAO.insertCustomer(customer);
		
		HttpSession session = req.getSession();
		session.setAttribute("uname", customerName);
		
		res.setContentType("text/html");
		res.getWriter().println("<font color=green><h1>"+result + " row(s) inserted</h1></font>");		
		res.getWriter().println("ID : " + customerId
				+ "<br> Name: " + customerName
				+ "<br> Address: "+ customerAddress
				+ "<br> Bill Amount: " + billAmount
				+ "<a href=SignOut> Enter </a>");
		
	}



}
