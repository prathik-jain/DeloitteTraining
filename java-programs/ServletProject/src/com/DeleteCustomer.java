package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;

/**
 * Servlet implementation class DeleteCustomer
 */
public class DeleteCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCustomer() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int customerId = Integer.parseInt(req.getParameter("customerId"));


		
		CustomerDAO customerDAO = new CustomerDAOImpl();
		if(customerDAO.isCustomerExists(customerId)) {
			int result = customerDAO.deleteCustomer(customerId);
			
			res.setContentType("text/html");
			res.getWriter().println("<font color=green><h1>"+result + " row(s) deleted</h1></font>");	
		
		} else {
			res.getWriter().println("Customer with "+customerId+ "does not exists" );
		}
	}

}
