package exe;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Tom
 */
public class Tom extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Tom() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	String name = req.getParameter("username");
    	String colors[] = req.getParameterValues("color");
    	
    	if(colors != null) {
    		for(String color: colors) {
    			resp.getWriter().println("<font color='"+color+"'> Hello, "+name+"<br>");
    		}
    		resp.getWriter().println("<a href=exeTom.html>TRY AGAIN</a>");
    	} else {
    		resp.getWriter().println("No color(s) selected <a href=exeTom.html>GO BACK </a>");
    	}
    }

}
