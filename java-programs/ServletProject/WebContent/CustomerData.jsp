<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="s" %>
<%@taglib uri="/WEB-INF/mytlds/address.tld" prefix="a" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<a:deloitteaddress></a:deloitteaddress>
<s:setDataSource var="ds" driver="oracle.jdbc.driver.OracleDriver" 
url="jdbc:oracle:thin:@localhost:1521:orcl"
scope="session" user="scott" password="tiger"/>

<s:query var="cust" dataSource="${ds}">
select customerId, customerName, customerAddress, billAmount from customer
</s:query>

<table border="1">
	<c:forEach var="row" items="${cust.rows}">
	<tr>
	<td><c:out value="${row.customerId}"/></td>
	<td><c:out value="${row.customerName}"/></td>
	<td><c:out value="${row.customerAddress}"/></td>
	<td><c:out value="${row.billAmount}"/></td>
	</tr>
	</c:forEach>
</table>
</body>
</html>