package com.project.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.project.spring.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public Boolean isCustomer(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int addCustomer(Customer customer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(customer);
//		logger.info("Person saved successfully, Person Details="+customer);
		return 0;
	}

	@Override
	public Customer getCustomerByNumber(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

}
