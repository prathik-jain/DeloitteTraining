package com.project.spring.dao;

import com.project.spring.model.Customer;

public interface CustomerDAO {
	
	public Boolean isCustomer(String phone);
	public int addCustomer(Customer customer);
	public Customer getCustomerByNumber(String phone);

}
