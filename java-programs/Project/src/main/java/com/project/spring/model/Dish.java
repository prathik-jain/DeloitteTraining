package com.project.spring.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name="Dish")
public class Dish {
	
	private int DishId;
	private int catId;
	private int typeId;
	private int price;
	private String desc;
	private int spice;
	private int imageId;
	
	public Dish() {}

	public Dish(int dishId, int catId, int typeId, int price, String desc, int spice, int imageId) {
		super();
		DishId = dishId;
		this.catId = catId;
		this.typeId = typeId;
		this.price = price;
		this.desc = desc;
		this.spice = spice;
		this.imageId = imageId;
	}
	
	@Id
	@Column(length=5)
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int getDishId() {
		return DishId;
	}

//	TODO: FK
	public int getCatId() {
		return catId;
	}

//	TODO: FK
	public int getTypeId() {
		return typeId;
	}

	@Column(length=5, nullable=false)
	public int getPrice() {
		return price;
	}

	@Column(length=255)
	public String getDesc() {
		return desc;
	}

	@Column(length=1)
//	TODO: add max and min
	public int getSpice() {
		return spice;
	}

//	TODO: FK
	public int getImageId() {
		return imageId;
	}

	public void setDishId(int dishId) {
		DishId = dishId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setSpice(int spice) {
		this.spice = spice;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	@Override
	public String toString() {
		return "Dish [DishId=" + DishId + ", catId=" + catId + ", typeId=" + typeId + ", price=" + price + ", desc="
				+ desc + ", spice=" + spice + ", imageId=" + imageId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + DishId;
		result = prime * result + catId;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + imageId;
		result = prime * result + price;
		result = prime * result + spice;
		result = prime * result + typeId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dish other = (Dish) obj;
		if (DishId != other.DishId)
			return false;
		if (catId != other.catId)
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (imageId != other.imageId)
			return false;
		if (price != other.price)
			return false;
		if (spice != other.spice)
			return false;
		if (typeId != other.typeId)
			return false;
		return true;
	}
	
	
	 
	 

}
