package com.project.spring.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Order")
public class Order {

	
	private int orderId;
	private int userId;
	private Timestamp startTime;
	private String comment;
	private Timestamp endTime;
	private String total;
	
	public Order() {
		super();
	}
	
	public Order(int orderId, int userId, Timestamp startTime, String comment, Timestamp endTime, String total) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.startTime = startTime;
		this.comment = comment;
		this.endTime = endTime;
		this.total = total;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int getOrderId() {
		return orderId;
	}

	@Column(length=20)
	public Timestamp getStartTime() {
		return startTime;
	}

	public int getUserId() {
		return userId;
	}
	
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	@Column(length=40)
	public String getComment() {
		return comment;
	}
	
	@Column(length=20)
	public Timestamp getEndTime() {
		return endTime;
	}

	@Column(length=20)
	public String getTotal() {
		return total;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	

	public void setTotal(String total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", userId=" + userId + ", startTime=" + startTime + ", comment=" + comment
				+ ", endTime=" + endTime + ", total=" + total + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + orderId;
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (orderId != other.orderId)
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

}
