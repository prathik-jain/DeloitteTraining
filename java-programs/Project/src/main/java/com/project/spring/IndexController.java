package com.project.spring;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	AnnotationConfiguration config = new AnnotationConfiguration().configure();
	Session session = config.buildSessionFactory().openSession();
	
	
	@RequestMapping("/")
	public String x() {
		return "login";
	}

}
