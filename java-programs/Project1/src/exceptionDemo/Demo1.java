package exceptionDemo;

import java.util.InputMismatchException;

import exceptions.NegativeNumberException;
import io.IO;

public class Demo1 {

	int num1, num2;
	float result;
	private void display() throws InterruptedException {
		
		try {
			System.out.println("Enter first number");
			num1= IO.read.nextInt();
			Thread.sleep(1000);
			System.out.println("Enter second number");
			num2=IO.read.nextInt();
			
			if(num1 < 0 || num2 < 0) {				
				try {
					throw new NegativeNumberException("Values are not possitive");
				} catch (NegativeNumberException e) {
					System.out.println(e.getMessage());
				}
			}
			result = (float)(num1/num2); 
			System.out.println("Result :" + result);
		} catch (InputMismatchException e) {
			System.out.println("Enter Integers only");
		} catch (ArithmeticException e) {	
			System.out.println("Cannot divide number by 0");
		}

		finally {
			System.out.println("finally");
		}
		
		
	} 
	public static void main(String[] args) throws InterruptedException {
		Demo1 d = new Demo1();
		d.display();
		System.out.println("Thank you for using the program");

	}

}
