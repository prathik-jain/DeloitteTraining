package inheritDemo;

public class Car extends Vehicle {

		public String carType; 

//		Overriding
//		- return type can be same / subclasses 
//		- Access specifiers should be same or more visible (private - default - protected - public

		public void start() {
			System.out.println("Car started");
		}
		
		public void showDetails() {
			super.showDetails();
			System.out.println("Car type: "+ carType);
		}

		
		
}
