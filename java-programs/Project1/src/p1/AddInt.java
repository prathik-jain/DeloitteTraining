package p1;

public class AddInt {

	int number1,number2, number3;
	
	public AddInt() {
		super();
	}

	public AddInt(int number1) {
		super();
		this.number1 = number1;
		this.number2 = 0;
	}

	public AddInt(int number1, int number2) {
		super();
		this.number1 = number1;
		this.number2 = number2;
	}

	public void setData(int num1, int num2) {
		this.number1 = num1;
		this.number2 = num2;
	}
	
	public void calculate() {
		this.number3 = this.number2 + this.number1;
	}
	
	public void display() {
		System.out.println("Sum = "+ number3);
	}
}
