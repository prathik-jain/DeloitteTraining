package p1;

public class IntArr {

	public static void main(String[] args) {
		int[] a = {1,2,3};
		
		IntegerArray b = new IntegerArray();
		System.out.println("No arg. size = " + b.arr.length);
		
//		Object with array
		IntegerArray arr = new IntegerArray(a);
		arr.display(0);
		arr.display();
		
//		Copy constructor
		IntegerArray x = new IntegerArray(arr);
		x.display(0);
		x.display();
	}

}
