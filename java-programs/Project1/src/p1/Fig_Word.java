package p1;

public class Fig_Word {

	
	public static String convert(long n) {
		String unit[] = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
				"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
		
		String tens[] = {"", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		
		String wval[] = {"crores","lacs","thousand","hundred","only"};
		
		long val[] = {10000000,100000,1000,100,1};
		String word = "";
		for (int i = 0; i < val.length; i++) {
			int n1 = (int)(n/val[i]);
			n = n%val[i];
			if (n1>0) {
				if(n1>20) {
					word += tens[n1/10] + " "+ unit[n1%10] + " "+  wval[i] + " ";
				} else {
					word += unit[n1] + " "+ wval[i]+ " ";
				}
			}
		}
		return word;
	}
	
}
