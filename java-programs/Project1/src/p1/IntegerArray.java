package p1;

public class IntegerArray {

	int[] arr;

	public IntegerArray() {
		super();
		this.arr = new int[10];
	}

	public IntegerArray(int size) {
		super();
		this.arr = new int[size];
	}
	
	public IntegerArray(int[] arr) {
		super();
		this.arr = arr;
	}

	public IntegerArray(IntegerArray a) {
		super();
		this.arr = a.arr;
	}
	
	public void display(int x) {
		for (int a: arr) {
			System.out.println(a);
		}
	}
	
	public void display() {
		for (int a: arr) {
			System.out.print(a+"\t");
		}
	}
	
}
