
package collectionDemo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Demo1 {

	public static void main(String[] args) {
		Set<String> batchList = new HashSet<String>();
		
		batchList.add("Kapoor");
		batchList.add("Kumar");
		batchList.add("Kapoor");
		batchList.add("Sharma");
		batchList.add("Kapoor");
		
		batchList.remove(2);
	
		System.out.println(batchList);
		
		Iterator<String> i = batchList.iterator();
		
		
		while (i.hasNext()) {
			System.out.println(i.next());
		}

	}

}
