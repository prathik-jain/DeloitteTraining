package exercise;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Exe3 {

	public static void main(String[] args) throws IOException {
		String rF = "data.txt";
		String wF = "newData.txt";
		
		File readFile = new File(rF);
		File writeFile = new File(wF);
		
		
		if(readFile.exists()) {
			FileWriter fw = new FileWriter(writeFile);
			FileReader fr = new FileReader(readFile);
			try {
				writeFile.createNewFile();
				int i;
				while((i =fr.read()) != -1) {
					fw.write((char)i);
				}
				System.out.println(rF + " copied to " + wF);
			}  catch (IOException e) {
				e.printStackTrace();
			} finally {
				fr.close();
				fw.close();
			}
			
		} else {
			System.out.println(rF + " does not exists");
			readFile.createNewFile();
		}

	}

}
