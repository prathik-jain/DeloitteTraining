package comparatorDemo;

import java.util.Comparator;

import getSet.Customer;

public class AddressComparator implements Comparator<Customer> {

	@Override
	public int compare(Customer o, Customer o1) {
		if(o.getCustomerAddress().compareTo(o1.getCustomerAddress()) > 0)
			return 1;
		else
			return -1;
	}
}
