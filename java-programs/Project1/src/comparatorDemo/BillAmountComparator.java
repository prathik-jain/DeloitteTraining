package comparatorDemo;

import java.util.Comparator;

import getSet.Customer;

public class BillAmountComparator implements Comparator<Customer> {
	
	@Override
	public int compare(Customer o, Customer o1) {
		if(o.getBillAmount() > o1.getBillAmount())
			return 1;
		else
			return -1;
	}
}
