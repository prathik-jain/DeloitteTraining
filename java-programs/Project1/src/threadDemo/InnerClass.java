package threadDemo;

public class InnerClass {

	int j=100;
	private class EncryptPassword{
		int i =90;
		private void display() {
			System.out.println("The value of i is: "+ i);
			System.out.println("The value of j is: "+ j);
		}
	}

	public static void main(String[] args) {
		InnerClass d = new InnerClass();
		InnerClass.EncryptPassword a = d.new EncryptPassword();
		
		a.display();
	}

}
