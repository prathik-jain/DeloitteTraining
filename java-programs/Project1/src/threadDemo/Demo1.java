package threadDemo;

public class Demo1 extends Thread{
	public Demo1() {
		Thread t1 = new Thread(this);
		t1.start();
	}
	
	@Override
	public void run() {
		super.run();
		System.out.println("Run called " +Thread.currentThread().getName());
	}
	public static void main(String[] args) {
		new Demo1();
		System.out.println("MAIN CALLED " +Thread.currentThread().getName() );
	}
}
