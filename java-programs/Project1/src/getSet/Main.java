package getSet;

public class Main {

	public static void main(String[] args) {
		Customer customer1 = new Customer(12,"Mohan", "Pune", 12600);
		Customer customer2 = new Customer(12,"Mohan", "Pune", 12600);
//		customer1.setCustomerAddress("Mumbai");
		System.out.println(customer1.toString());
		System.out.println(customer2);
		System.out.println(customer1.hashCode() == (customer2.hashCode()));
		System.out.println(customer1 == customer2); //false
		System.out.println(customer1.equals(customer2)); //false
		System.out.println(Integer.toHexString(customer1.hashCode()));
	}

}
