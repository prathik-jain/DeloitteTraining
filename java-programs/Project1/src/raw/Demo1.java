package raw;

public class Demo1<Z> {

	public static void main(String[] args) {
		
		Demo1<Boolean> d = new Demo1<Boolean>();
		d.display(true);
		
		Demo1<String> d1 = new Demo1<String>();
		d1.display("Hello");
		
		System.out.println(d1.add(5, 10));
		
		System.out.println(d1.add(1.2, 2.2));

		System.out.println(d1.add(1.2, 2.2));
	}

	private void display(Z a) {
		System.out.println(a);
	}

	
	private <Z extends Number> double add(Z a, Z b) {
			return a.doubleValue()+b.doubleValue();
	}
}
