<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<f:form action="CustomerInfo.do">
Customer ID: <f:input path="customerId"/> <br>
Customer Name: <f:input path="customerName"/> <br>
Customer Address: <f:input path="customerAddress"/> <br>
Bill Amount: <f:input path="billAmount"/> <br>
<input type="submit" values="Insert">
</f:form>

</body>
</html>