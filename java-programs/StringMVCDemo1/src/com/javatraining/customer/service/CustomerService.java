package com.javatraining.customer.service;

import java.util.List;

import com.javatraining.customer.model.Customer;

public interface CustomerService {
	public int insertCustomer(Customer customer);
	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount);
	public int deleteCustomer(int customerId);
	public Customer findByCustomerId(int customerId);
	public boolean isCustomerExists(int customerId);
	public List<Customer> listAllCustomers();
}
