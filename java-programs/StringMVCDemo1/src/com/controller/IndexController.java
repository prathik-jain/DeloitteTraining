package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.model.Customer;

@Controller
public class IndexController {

	@RequestMapping("/customerF")
	public ModelAndView getGuestView() {
	
		ModelAndView view = new ModelAndView();
		view.setViewName("customerForm");
//		command object for spring from
		view.addObject("command", new Customer());
		
		return view;
	}
	
}
