package com.model;

import java.io.Serializable;

public class Guest implements Serializable{

	private String guestName;
	
	public Guest() {
		// TODO Auto-generated constructor stub
	}

	public Guest(String guestName) {
		super();
		this.guestName = guestName;
	}

	@Override
	public String toString() {
		return "Guest [guestName=" + guestName + "]";
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guestName == null) ? 0 : guestName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Guest other = (Guest) obj;
		if (guestName == null) {
			if (other.guestName != null)
				return false;
		} else if (!guestName.equals(other.guestName))
			return false;
		return true;
	}
	
	
}
