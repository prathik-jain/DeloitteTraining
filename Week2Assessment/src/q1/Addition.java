/**
 * 
 */
package q1;

/**
 * @author nexwave
 *
 */
public class Addition extends Arithmetic {

	/* (non-Javadoc)
	 * @see q1.Arithmetic#CalculatedValue()
	 */
	@Override
	public void calculate() {
		this.num3 = this.num1 + this.num2;
	}

}
