/**
 * 
 */
package q1;

/**
 * @author nexwave
 *
 */
public class Multiplication extends Arithmetic {

	@Override
	public void calculate() {
		this.num3 = this.num1 * this.num2;
	}

	/* (non-Javadoc)
	 * @see q1.Arithmetic#CalculatedValue()
	 */


}
