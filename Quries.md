## Queries


1. Display ID and name in caps where the first character is 'R'

```sql 
select stud_id, upper(f_name) from s_details where upper(f_name) like 'R%';
```

```

   STUD_ID UPPER(F_NAME)
---------- --------------------
         1 ROHIT
         2 RAHUL

```

###### Pattern
	R% - starts with R
	%L - ends with L
	_O% - select char O and anything

2. Display ID, marks, total, average and result

```sql
select stud_id ID, m1, m2, m3, m1+m2+m3 as total, (m1+m2+m3/3) as "average",
case when m1<35 or m2<35 or m3<35 then 'FAIL'
when (m1+m2+m3)<50 then 'PASS'
when (m1+m2+m3)<60 then 'Second Class'
when (m1+m2+m3)<80 then 'First Class'
else 'Distinction' 
end as "Result" from marks;
```

```

        ID         M1         M2         M3      TOTAL    average Result
---------- ---------- ---------- ---------- ---------- ---------- ------------
         1         50         60         70        180 133.333333 Distinction
         2         68         78         70        216 169.333333 Distinction
         3         45         78         70        193 146.333333 Distinction
         4         77         37         70        184 137.333333 Distinction
         5         57         68         70        195 148.333333 Distinction
         6         87         23         70        180 133.333333 FAIL
         7         75         98         70        243 196.333333 Distinction
         8         56         63         70        189 142.333333 Distinction
         1         35         60         70        165 118.333333 Distinction
         3         55         35         35        125 101.666667 Distinction
         4         58         57         46        161 130.333333 Distinction

```

3. Display ID, total marks and test name

```sql
select stud_id, (m1+m2+m3) as total, decode(test, 1, 'First test', 2, 'Second test', 3, 'Thrid test', 'Others') "Test Name" from marks;
```

```

   STUD_ID      TOTAL Test Name
---------- ---------- -----------
         1        180 First test
         2        216 First test
         3        193 First test
         4        184 First test
         5        195 First test
         6        180 First test
         7        243 First test
         8        189 First test
         1        165 Second test
         3        125 Second test
         4        161 Second test
```

### Joins
1. Display student ID, student f_name, marks1 for first test
	
```sql
select a.stud_id, a.f_name, b.m1 from s_details a inner join marks b on (a.stud_id = b.stud_id and b.test = 1);
```

```

   STUD_ID F_NAME                       M1
---------- -------------------- ----------
         1 rohit                        50
         2 rahul                        68
         3 noel                         45
         4 kp                           77
         5 paresh                       57
         6 barjindar                    87
         7 shreya                       75
         8 mohit                        56

```

This is a equijoin

2. Display student ID, student f_name, marks1 for other than first and second test

```sql
select a.stud_id, a.f_name, b.m1 from s_details a inner join marks b on (a.stud_id = b.stud_id and b.test > 2);
```
	
```no rows selected```
		
This is non-equijoin 

3. Display student ID, student f_name, marks1 for first test if a student has not taken the test display absent (Outer join - as the query has a negation)
	
```sql
select a.stud_id, a.f_name, nvl(to_char(b.m1),'Absent') from s_details a left outer join marks b on (a.stud_id = b.stud_id and b.test = 1);
```

```
	
   STUD_ID F_NAME               NVL(TO_CHAR(B.M1),'ABSENT')
---------- -------------------- ----------------------------------------
         1 rohit                50
         2 rahul                68
         3 noel                 45
         4 kp                   77
         5 paresh               57
         6 barjindar            87
         7 shreya               75
         8 mohit                56
         9 priyanka             Absent
        10 samuel               Absent
```

4. Display studnet ID, f_name and city

```sql
select a.stud_id, a.f_name, b.c_name from s_details a
inner join conn c on c.stud_id = a.stud_id
inner join city b on b.c_id = c.c_id; 	
```

```
   STUD_ID F_NAME               C_NAME
---------- -------------------- -------------------------
         1 rohit                chennai
         3 noel                 surat
         4 kp                   chennai
         5 paresh               bangalore
         6 barjindar            mumbai
         7 shreya               punjab
         8 mohit                bangalore
         9 priyanka             delhi
        10 samuel               tiruwanantpuram
```


5. Display student id, fname, city, and state

```sql
select a.stud_id, a.f_name, b.c_name, s.st_name from s_details a
inner join conn c on c.stud_id = a.stud_id
inner join city b on b.c_id = c.c_id	
inner join state s on s.st_id = b.s_id; 	
```
	
```
	   STUD_ID F_NAME               C_NAME                    ST_NAME
---------- -------------------- ------------------------- -------------------------
         1 rohit                chennai                   tamil nadu
         3 noel                 surat                     gujrat
         4 kp                   chennai                   tamil nadu
         5 paresh               bangalore                 karnataka
         6 barjindar            mumbai                    maharastra
         7 shreya               punjab                    haryana
         8 mohit                bangalore                 karnataka
         9 priyanka             delhi                     delhi
        10 samuel               tiruwanantpuram           kerala

```

6. Display studnet ID , fname and subject opted for

```sql
select a.stud_id, a.f_name, (select sub_name from sub where sub_id = c.subid1) ,
(select sub_name from sub where sub_id = c.subid2),
(select sub_name from sub where sub_id = c.subid3) from s_details a
inner join conn c on c.stud_id = a.stud_id;
```

```
	
	   STUD_ID F_NAME               (SELECTSUB_NAMEFROMSUBWHE (SELECTSUB_NAMEFROMSUBWHE (SELECTSUB_NAMEFROMSUBWHE
---------- -------------------- ------------------------- ------------------------- -------------------------
         1 rohit                science                   hindi                     maths
         3 noel                 hindi                     science                   science
         4 kp                   science                   english                   hindi
         5 paresh               maths                     science                   kannada
         6 barjindar            kannada                   maths                     english
         7 shreya               english                   kannada                   science
         8 mohit                science                   science                   english
         9 priyanka             maths                     hindi                     hindi
        10 samuel               hindi                     maths                     science

```

7. Display studentID , name, subject name and marks1 for first test

```sql
select a.stud_id, a.f_name, 
(select sub_name from sub where sub_id = c.subid1), m.m1 ,
(select sub_name from sub where sub_id = c.subid2), m.m2,
(select sub_name from sub where sub_id = c.subid3), m.m3
from s_details a
inner join conn c on (c.stud_id = a.stud_id)
inner join marks m on (m.stud_id = a.stud_id and m.test =1);
```
	
	
```
   STUD_ID F_NAME               (SELECTSUB_NAMEFROMSUBWHE         M1 (SELECTSUB_NAMEFROMSUBWHE         M2 (SELECTSUB_NAMEFROMSUBWHE         M3
---------- -------------------- ------------------------- ---------- ------------------------- ---------- ------------------------- ----------
         1 rohit                science                           50 hindi                             60 maths                             70
         3 noel                 hindi                             45 science                           78 science                           70
         4 kp                   science                           77 english                           37 hindi                             70
         5 paresh               maths                             57 science                           68 kannada                           70
         6 barjindar            kannada                           87 maths                             23 english                           70
         7 shreya               english                           75 kannada                           98 science                           70
         8 mohit                science                           56 science                           63 english                           70

```

### Subquries

1. Display name of students who has taken first test

```sql
select stud_id, f_name from s_details
where stud_id in (select stud_id from marks where test =1 );
```

```
   STUD_ID F_NAME
---------- --------------------
         1 rohit
         2 rahul
         3 noel
         4 kp
         5 paresh
         6 barjindar
         7 shreya
         8 mohit
```

2. List the names of students who stay in bangalore

```sql
select stud_id, f_name from s_details where
stud_id in (select stud_id from conn where
conn.c_id in (select c_id from city where
upper(c_name) = 'BANGALORE'));
```

```
   STUD_ID F_NAME
---------- --------------------
         5 paresh
         8 mohit
```

3. List the names of people in karnataka

```sql
select stud_id, f_name from s_details where
stud_id in (select stud_id from conn where
conn.c_id in (select c_id from city where
s_id in (select st_id from state where
upper(st_name) = 'KARNATAKA')));
```

```
   STUD_ID F_NAME
---------- --------------------
         5 paresh
         8 mohit
```


4. List names who has taken English as a subject

```sql
SELECT stud_id, f_name from s_details where
stud_id in (select stud_id from conn where
subid1 in (select sub_id from sub where upper(sub_name) = 'ENGLISH') or
subid2 in (select sub_id from sub where upper(sub_name) = 'ENGLISH') or
subid3 in (select sub_id from sub where upper(sub_name) = 'ENGLISH'));
```

```
   STUD_ID F_NAME
---------- --------------------
         2 rahul
         4 kp
         6 barjindar
         7 shreya
         8 mohit
```
5. List names who has not taken math as a subject

```sql
SELECT stud_id, f_name from s_details where
stud_id not in (select stud_id from conn where
subid1 in (select sub_id from sub where upper(sub_name) = 'MATH') or
subid2 in (select sub_id from sub where upper(sub_name) = 'MATH') or
subid3 in (select sub_id from sub where upper(sub_name) = 'MATH'));
```

```
   STUD_ID F_NAME
---------- --------------------
         1 rohit
         2 rahul
         3 noel
         4 kp
         5 paresh
         6 barjindar
         7 shreya
         8 mohit
         9 priyanka
        10 samuel
```

6. List names whose total marks is greater than rahul's total marks

```sql
select stud_id, f_name from s_details where
stud_id in ( 
select stud_id from marks where
(m1+m2+m3) > any (select (m1+m2+m3) from marks where
stud_id in ( select stud_id from s_details where upper(f_name) = 'RAHUL')));
```

```

   STUD_ID F_NAME
---------- --------------------
         7 shreya
```

7. List names who has failed in the first test

```sql 
select stud_id, f_name from s_details where
stud_id in (select stud_id from marks where ((m1<35 or m2<35 or m3<35) and test =1));
```

```
   STUD_ID F_NAME
---------- --------------------
         6 barjindar

```


### Multirow Function

1. Display stud_id, f_name with number of test he has taken and avg performance

```sql
select a.stud_id, a.f_name, b.nos, b.per from s_details a 
inner join 
(select stud_id, count(*) nos, avg((m1+m2+m3)/3) per from marks group by stud_id) b on (a.stud_id = b.stud_id);
```

```

   STUD_ID F_NAME                      NOS        PER
---------- -------------------- ---------- ----------
         1 rohit                         2       57.5
         6 barjindar                     1         60
         2 rahul                         1         72
         4 kp                            2       57.5
         5 paresh                        1         65
         8 mohit                         1         63
         3 noel                          2         53
         7 shreya                        2 63.1666667
        10 samuel                        1 41.3333333
```


2. Give the list of city with the number of students staying in a particular city

```sql
select  c.c_name, count(*) from s_details a
inner join conn b on b.stud_id = a.stud_id
inner join city c on c.c_id = b.c_id
group by  c.c_name;
```

```
C_NAME                      COUNT(*)
------------------------- ----------
tiruwanantpuram                    1
chennai                            2
mumbai                             1
delhi                              1
punjab                             1
bangalore                          2
patna                              1
surat                              1
```

3. Give a list of state with number of students stay in each state

```sql 
select  s.st_name, count(*) from s_details a
inner join conn b on b.stud_id = a.stud_id
inner join city c on c.c_id = b.c_id
inner join state s on s.st_id = c.s_id
group by  s.st_name;
```

```
ST_NAME                     COUNT(*)
------------------------- ----------
karnataka                          2
maharastra                         1
delhi                              1
tamil nadu                         2
gujrat                             1
haryana                            1
kerala                             1
bihar                              1
```

4. List all the subject with total number of sudents in each subject


```sql
select  s.sub_name, a.stud_id, count(*) from s_details a
inner join conn b on b.stud_id = a.stud_id
inner join sub s 
on s.sub_id = b.subid1
or s.sub_id = b.subid2
or s.sub_id = b.subid3 
group by  s.sub_name, a.stud_id;
```

```
SUB_NAME                    COUNT(*)
------------------------- ----------
english                            5
maths                              5
hindi                              5
kannada                            3
social science                     1
science                            8
```

5. Display in each month how many students are born

```sql
```

6. Display in each day of week how many People are born

